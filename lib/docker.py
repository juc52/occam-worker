import os
import json
import base64
import time

class Docker:
  @staticmethod
  def docker_name(obj_type, obj_name, obj_revision=""):
    obj_type     = base64.b32encode(obj_type.encode('utf-8')).lower().replace('='.encode('utf-8'), '-'.encode('utf-8')).decode('utf-8')
    obj_name     = base64.b32encode(obj_name.encode('utf-8')).lower().replace('='.encode('utf-8'), '-'.encode('utf-8')).decode('utf-8')
    obj_revision = base64.b32encode(obj_revision.encode('utf-8')).lower().replace('='.encode('utf-8'), '-'.encode('utf-8')).decode('utf-8')

    hashed = "%s-%s" % (obj_type, obj_name)

    if not obj_revision == "":
      hashed = "%s-%s" % (hashed, obj_revision)

    return hashed

  @staticmethod
  def console(opts, obj_type, obj_name, obj_revision=""):
    if obj_type is None:
      # pull from object.json if it exists
      f = open("object.json")
      info = json.load(f)
      f.close()

      obj_revision = ""

      if 'type' in info:
        obj_type     = info['type']
      if 'name' in info:
        obj_name     = info['name']
      if 'revision' in info:
        obj_revision = info['revision']

    hashed_name = Docker.docker_name(obj_type, obj_name, obj_revision)

    # Collect volumes
    volumes = []
    if "input" in info:
      for input_object_type, objects in info["input"].items():
        for object in objects:
          if "using" in object:
            if "type" in object["using"] and "name" in object["using"] and "id" in object["using"] and "revision" in object["using"]:
              if 'type' in object['using']:
                input_obj_type     = object['using']['type']
              if 'name' in object['using']:
                input_obj_name     = object['using']['name']
              if 'revision' in object['using']:
                input_obj_revision = object['using']['revision']
              hashed_super_object_name = Docker.docker_name(input_obj_type,
                                                            input_obj_name,
                                                            input_obj_revision)
              volumes.append("occam-%s" % (hashed_super_object_name))
          input_obj_revision = ""
          if 'type' in object:
            input_obj_type     = object['type']
          if 'name' in object:
            input_obj_name     = object['name']
          if 'revision' in object:
            input_obj_revision = object['revision']
          hashed_input_object_name = Docker.docker_name(input_obj_type,
                                                        input_obj_name,
                                                        input_obj_revision)
          volumes.append("occam-%s" % (hashed_input_object_name))

    print(volumes)
    volumes.append("occam-trace-workflow-foo")
    volumes = [("--volumes-from %s" % (x)) for x in volumes]
    volumes_args = ' '.join(volumes)

    volume = ""
    if opts.volume:
      volume = "-v %s" % (opts.volume)
    if opts.job:
      volume = "-v %s:/job:rw" % (os.path.realpath(os.curdir))
    print("Docker name: %s" % hashed_name)
    os.system("docker run -i -t %s %s occam/%s /bin/bash -c 'cd occam/%s-%s; /bin/bash'" % (volume, volumes_args, hashed_name, obj_type, obj_name))

  @staticmethod
  def list(opts, object_type):
    sim_tag = base64.b32encode(object_type.encode('utf-8')).lower().replace('='.encode('utf-8'), '-'.encode('utf-8')).decode('utf-8')
    os.system("docker images | grep -e '^occam/%s-' " % (sim_tag))

  @staticmethod
  def run(obj_type, obj_name, obj_revision=""):
    f = open("object.json")
    info = json.load(f)
    f.close()

    if obj_type is None:
      obj_revision = ""

      if 'type' in info:
        obj_type     = info['type']
      if 'name' in info:
        obj_name     = info['name']
      if 'revision' in info:
        obj_revision = info['revision']

    hashed_name = Docker.docker_name(obj_type, obj_name, obj_revision)

    # Collect volumes
    volumes = []
    if "input" in info:
      for input_object_type, objects in info["input"].items():
        for object in objects:
          if "using" in object:
            if "type" in object["using"] and "name" in object["using"]:
              input_obj_id = ""
              input_obj_revision = ""
              if 'type' in object['using']:
                input_obj_type     = object['using']['type']
              if 'name' in object['using']:
                input_obj_name     = object['using']['name']
              if 'revision' in object['using']:
                input_obj_revision = object['using']['revision']
              hashed_super_object_name = Docker.docker_name(input_obj_type,
                                                            input_obj_name,
                                                            input_obj_revision)
              print("Adding volume for %s-%s" % (input_obj_type, input_obj_name))
              volumes.append("occam-%s" % (hashed_super_object_name))

          input_obj_revision = ""
          if 'type' in object:
            input_obj_type     = object['type']
          if 'name' in object:
            input_obj_name     = object['name']
          if 'revision' in object:
            input_obj_revision = object['revision']
          hashed_input_object_name = Docker.docker_name(input_obj_type,
                                                        input_obj_name,
                                                        input_obj_revision)
          print("Adding volume for %s-%s" % (input_obj_type, input_obj_name))
          volumes.append("occam-%s" % (hashed_input_object_name))

    print(volumes)
    volumes.append("occam-trace-workflow-foo")
    volumes = [("--volumes-from %s" % (x)) for x in volumes]
    volumes_args = ' '.join(volumes)
    command = "docker run -v %s:/job:rw %s -t occam/%s" % (os.path.realpath(os.curdir), volumes_args, hashed_name)
    print("Running docker: %s" % (command))
    os.system(command)
    print("Found output.json? %s" % os.path.exists("%s/output.json" % (os.path.realpath(os.curdir))))

  @staticmethod
  def build(opts, obj_type, obj_name, obj_revision=""):
    path = "."
    if not opts.git is None:
      os.system("git clone %s package" % (opts.git))
      path = "./package" % (name)

    f = open("%s/object.json" % (path))
    info = json.load(f)
    f.close()

    if obj_type is None:
      obj_revision = ""
      obj_id = ""

      if 'type' in info:
        obj_type     = info['type']
      if 'name' in info:
        obj_name     = info['name']
      if 'revision' in info:
        obj_revision = info['revision']

    hashed_name = Docker.docker_name(obj_type, obj_name, obj_revision)

    if not 'install' in info:
      info['install'] = {}

    # Overrides
    if not opts.source_git is None:
      info['install']['git'] = opts.source_git
      info['install']['hg']  = None
      info['install']['svn'] = None
      info['install']['tar'] = None
    elif not opts.source_svn is None:
      info['install']['svn'] = opts.source_svn
      info['install']['hg']  = None
      info['install']['tar'] = None
      info['install']['git'] = None
    elif not opts.source_tar is None:
      info['install']['tar'] = opts.source_tar
      info['install']['hg']  = None
      info['install']['svn'] = None
      info['install']['git'] = None
    elif not opts.source_hg is None:
      info['install']['hg']  = opts.source_hg
      info['install']['tar'] = None
      info['install']['svn'] = None
      info['install']['git'] = None

    if info['type'] != "base":
      # Build Dockerfile
      f = open("%s/Dockerfile" % (path), 'w+')
      f.write("# DOCKER-VERSION 0.11.1\n")

      using = info['build']['using']
      using_revision = ""

      if isinstance(using, dict):
        using_type = using["type"]
        using_name = using["name"]
        if "revision" in using:
          using_revision = using["revision"]
      else:
        using_type = "base"
        using_name = using

      root = "/occam/%s-%s" % (obj_type, obj_name)
      root = root.replace(' ', '-')
      print("Name: %s" % obj_name)

      hashed_using_name = Docker.docker_name(using_type, using_name, using_revision)

      f.write("FROM occam/%s\n" % (hashed_using_name))

      # Create a Docker volume
      f.write("ADD . %s\n" % (root))

      # Run build script
      if "build" in info:
        # Install build dependencies
        if "packages" in info["build"]:
          for library in info["build"]["packages"]:
            f.write("RUN echo 'y' | apt-get install %s\n" % (library))

        if "script" in info["build"]:
          language = info["build"]["language"]
          filename = info["build"]["script"]
          if "version" in info["build"]:
            version = info["build"]["version"]
          else:
            version = None

          if language == "python":
            if not version is None:
              script = "pyenv local %s; python %s/%s" % (version, root, filename)
            else:
              script = "pyenv local %s; python %s/%s" % ('2.7.6', root, filename)
          elif language == "ruby":
            if not version is None:
              script = "rbenv local %s; ruby %s/%s" % ('2.0.0', root, filename)
            else:
              script = "rbenv local %s; ruby %s/%s" % (version, root, filename)
          elif language == "perl":
            if not version is None:
              script = "plenv local %s; perl %s/%s" % ('5.18.0', root, filename)
            else:
              script = "plenv local %s; perl %s/%s" % (version, root, filename)
          elif language == "bash":
            script = "bash %s/%s" % (root, filename)
          elif language == "sh":
            script = "sh %s/%s" % (root, filename)

          f.write("RUN cd %s; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd %s; %s'\n" % (root, root, script))

      # Write out the volume
      f.write('VOLUME ["%s"]\n' % (root))

      command = ""

      # Determine the object's run command if it exists
      run_cmd = '/bin/true'
      run_root = root

      if "run" in info:
        if "index" in info["run"] and "type" in info["run"]:
          run_type = info["run"]["type"]
          index = info["run"]["index"]
          # Add run commands for any object input inits that need to happen.
          if "input" in info:
            input_object_info = info["input"][run_type][index]
            if "input" in input_object_info:
              for input_object_type, input_object_indexes in input_object_info["input"].items():
                for input_object_index in input_object_indexes:
                  object = info["input"][input_object_type][input_object_index]
                  input_name = object["name"]
                  # Does this object have an initialization routine?
                  if "init" in object:
                    print("Creating an initialization action for %s %s" % (input_object_type, input_name))
                    init_root = "/occam/%s-%s" % (input_object_type, input_name)
                    init_root = init_root.replace(' ', '-')
                    if "script" in object["init"]:
                      # Create a run action, and run it to build
                      language = object["init"]["language"]
                      filename = object["init"]["script"]
                      if "version" in object["init"]:
                        version = object["init"]["version"]
                      else:
                        version = None

                      if language == "python":
                        if not version is None:
                          script = "pyenv local %s; python %s/%s" % (version, init_root, filename)
                        else:
                          script = "pyenv local %s; python %s/%s" % ('2.7.6', init_root, filename)
                      elif language == "ruby":
                        if not version is None:
                          script = "rbenv local %s; ruby %s/%s" % ('2.0.0', init_root, filename)
                        else:
                          script = "rbenv local %s; ruby %s/%s" % (version, init_root, filename)
                      elif language == "perl":
                        if not version is None:
                          script = "plenv local %s; perl %s/%s" % ('5.18.0', init_root, filename)
                        else:
                          script = "plenv local %s; perl %s/%s" % (version, init_root, filename)
                      elif language == "bash":
                        script = "bash %s/%s" % (init_root, filename)
                      elif language == "sh":
                        script = "sh %s/%s" % (init_root, filename)

                      print("Adding init command")
                      command = command + script + (" %s %s; " % (input_object_type, input_object_index))

        if "script" in info["run"]:
          language = info["run"]["language"]
          filename = info["run"]["script"]
          if "version" in info["build"]:
            version = info["run"]["version"]
          else:
            version = None

          if "name" in info["run"]:
            run_name = info["run"]["name"]

            if "type" in info["run"]:
              run_type = info["run"]["type"]

              run_root = "/occam/%s-%s" % (run_type, run_name)
              run_root = run_root.replace(' ', '-')

          if language == "python":
            if not version is None:
              run_cmd = "pyenv local %s; python %s/%s" % (version, run_root, filename)
            else:
              run_cmd = "pyenv local %s; python %s/%s" % ('2.7.6', run_root, filename)
          elif language == "ruby":
            if not version is None:
              run_cmd = "rbenv local %s; ruby %s/%s" % ('2.0.0', run_root, filename)
            else:
              run_cmd = "rbenv local %s; ruby %s/%s" % (version, run_root, filename)
          elif language == "perl":
            if not version is None:
              run_cmd = "plenv local %s; perl %s/%s" % ('5.18.0', run_root, filename)
            else:
              run_cmd = "plenv local %s; perl %s/%s" % (version, run_root, filename)
          elif language == "bash":
            run_cmd = "bash %s/%s" % (run_root, filename)
          elif language == "sh":
            run_cmd = "sh %s/%s" % (run_root, filename)

      run_cmd = "cd %s; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /job; %s %s'" % (run_root, command, run_cmd)

      f.write('CMD %s\n' % (run_cmd))

      f.close()

    print("Building %s %s" % (obj_type, obj_name))

    # We will also need to create the output container
    # TODO: base on "output"
    if "output" in info:
      print("Building %s" % ("trace"))
      os.system("docker run -v /occam/trace --name occam-trace-workflow-foo -t occam/mjqxgzi--ovrhk3tuouwweyltmuwtembrgqwtanq- /bin/true")

    print("Building %s (%s)" % (obj_name, hashed_name))
    print("docker build --rm=true -t occam/%s %s" % (hashed_name, path))
    os.system("docker rm occam-%s" % (hashed_name))
    os.system("docker build --rm=true -t occam/%s %s" % (hashed_name, path))
    os.system("docker run --name occam-%s -t occam/%s /bin/true" % (hashed_name, hashed_name))
