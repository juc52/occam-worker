import os
import json
import threading
import time
from datetime import date

from models.job              import Job
from models.system           import System
from models.job_dependency   import JobDependency
from models.job_codependency import JobCodependency

from time import sleep

from lib.document_store import DocumentStore
from lib.db             import DB
from lib.builder        import Builder
from lib.installer      import Installer
from lib.runner         import Runner
from lib.daemon         import Daemon
from lib.signal         import Signal
from lib.memcache       import Memcache
from lib.monitor        import Monitor

class Worker:
  def __init__(self, options):
    self.options = options

    DB.initialize(self.options)

    self.system = DB.session().query(System).first()

    if self.system is None:
      # Panic
      print("OCCAM System not initialized. Run configuration in a web browser first.")
      exit(-1)
      return

    if self.options.rootPath is None:
      self.options.rootPath = self.system.jobs_path

    self.options.objectPath = self.system.objects_path

    if not os.path.exists(self.options.rootPath):
      print("Making jobs directory: %s" % (self.options.rootPath))
      os.mkdir(self.options.rootPath)

    self.workersPath = "%s/workers" % (self.options.rootPath)
    self.runningPath = "%s/running" % (self.options.rootPath)

    if not os.path.exists(self.workersPath):
      print("Making workers directory: %s" % (self.workersPath))
      os.mkdir(self.workersPath)

    if not os.path.exists(self.runningPath):
      print("Making running directory: %s" % (self.runningPath))
      os.mkdir(self.runningPath)

    if self.options.daemon:
      DB.close()
      retCode = Daemon(self.options.rootPath, "%s/occam-worker" % (self.workersPath), True).create()
      # Reopen the database in the daemon process
      DB.initialize(self.options)

    # Open document store
    DocumentStore.initialize(self.options)

    # Open memcache access
    Memcache.initialize(self.options)

    self.lock = threading.Lock()
    # TODO: do this better. python doesn't let us release a lock more than
    #       once. wtf.
    self.lock_lock = threading.Lock()

    self.signaller = Signal({
      'finished': self.signal,
      'worker':   self,
    })

    retCode = 0

    procParams = """
    {
      "exit_code":          "%s",
      "process_id":         "%s",
      "parent_process_id":  "%s",
      "parent_group_id":    "%s",
      "session_id":         "%s",
      "user_id":            "%s",
      "effective_user_id":  "%s",
      "real_group_id":      "%s",
      "effective_group_id": "%s"
    }
    """ % (retCode, os.getpid(), os.getppid(), os.getpgrp(), os.getsid(0),
                    os.getuid(), os.geteuid(), os.getgid(),  os.getegid())

    open("%s/occam-worker-%s.json" % (self.workersPath, os.getpid()), "w").write(procParams + "\n")

  def createJobPath(self, job):
    now = date.today()

    year_path  = "%s/%s" % (self.options.rootPath, now.year)
    month_path = "%s/%s" % (year_path,             now.month)
    day_path   = "%s/%s" % (month_path,            now.day)
    job_path   = "%s/job-%s" % (day_path,              job.id)

    if not os.path.exists(year_path):
      print("Making year directory: %s" % (year_path))
      os.mkdir(year_path)

    if not os.path.exists(month_path):
      print("Making month directory: %s" % (month_path))
      os.mkdir(month_path)

    if not os.path.exists(day_path):
      print("Making day directory: %s" % (day_path))
      os.mkdir(day_path)

    if not os.path.exists(job_path):
      print("Making job directory: %s" % (job_path))
      os.mkdir(job_path)

    return job_path

  def run(self):
    while True:
      if not self.idle():
        if self.options.verbose:
          print("No work")
        sleep(3)

  # Called when the worker is idle
  def idle(self):
    # Yank all jobs from the database
    for job in DB.session().query(Job).filter_by(status           = "queued",
                                                 codependant      = 0,
                                                 has_dependencies = 0):
      self.perform(job)
      return True

    return False

  # Initialize the jobs
  def initJob(self, job):
    # Print to log
    print("Started job #" + str(job.id))

    # Update job status to running
    job.status = "running"

    # Establish start time
    start_time = time.time()

    # Create a directory for the job to run in with the format:
    # {occam-root}/%yyyy/%mm/%dd/job-{job id}
    jobWorkingPath = self.createJobPath(job)

    # Afterward, this directory will have a 'job-{job id}.json' and a
    # 'job-{job id}.log' which details its running environment and its
    # output respectively.

    # Determine who acts for this job
    if (job.occam_object_id != None):
      if job.kind == "install":
        print("Object install job")
        self.tasks.append([job, Installer(self.options, jobWorkingPath)])
      elif job.kind == "build":
        print("Object build job")
        self.tasks.append([job, Builder(self.options, jobWorkingPath)])
    else:
      print("Object run job")
      self.tasks.append([job, Runner(self.options, jobWorkingPath)])

    # Create job running file
    print("Writing file")
    jobFile = "%s/%s" % (self.runningPath, str(job.id))
    open(jobFile, "w+").close()
    self.files.append([job, jobFile])
    print("Ok")

  # Do the given job
  def perform(self, job):
    # Remember the current job
    self.job = job
    self.tasks = []
    self.files = []

    # Set up wait semaphore
    self.lock.acquire()

    # We need to spawn all co-dependant jobs
    codependants = DB.session().query(Job).filter(Job.id.in_(DB.session().query(JobCodependency.depends_on_job_id).filter_by(job_id = job.id)))
    for codependant in codependants:
      print("start")
      self.initJob(codependant)
      print("end")

    self.initJob(job)

    # Create a directory for the job to run in with the format:
    # {occam-root}/%yyyy/%mm/%dd/job-{job id}
    jobWorkingPath = self.createJobPath(job)

    # Tell world we are starting these jobs
    DB.session().commit()

    jobInfoPath = "%s/current.json" % (self.options.rootPath)
    jobLogFilePath = "%s/job-%s.log" % (jobWorkingPath, job.id)
    outputLogFilePath = "%s/output.raw" % (jobWorkingPath)
    open(outputLogFilePath, "w+").close()

    # Run the jobs
    for task in self.tasks:
      if not task[1].run(task[0]):
        self.fail(task[0])

    def tail(f, window=20):
      """
      Returns the last `window` lines of file `f` as a list.
      """
      if window == 0:
        return []
      BUFSIZ = 1024
      f.seek(0, 2)
      bytes = f.tell()
      size = window + 1
      block = -1
      data = []
      while size > 0 and bytes > 0:
        if bytes - BUFSIZ > 0:
          # Seek back one whole BUFSIZ
          f.seek(block * BUFSIZ, 2)
          # read BUFFER
          data.insert(0, f.read(BUFSIZ).decode("utf-8"))
        else:
          # file too small, start from begining
          f.seek(0,0)
          # only read what was not read
          data.insert(0, f.read(bytes).decode("utf-8"))

        linesFound = data[0].count('\n')
        size -= linesFound
        bytes -= BUFSIZ
        block -= 1
      # Remove everything up to the first newline
      data = ''.join(data)
      return data[(data.find('\n')+1):]

    child_pid = None
    monitor = None
    stats = {}

    # Wait for signal meaning the job is done
    print("Waiting")

    # Wait until the semaphore is unlocked
    # Look at all children
    while self.lock.locked():
      if child_pid is None:
        try:
          spawned = json.load(open(jobInfoPath))
          child_pid = spawned["process_id"]
          monitor = Monitor(child_pid)
          print("Child process: %s" % (child_pid))
        except:
          child_pid = None

      if child_pid is not None:
        # Monitoring/output pushing
        stats = monitor.status()

      # Tail the job output to memcache
      if os.path.exists(jobLogFilePath):
        jobFile = open(jobLogFilePath, "rb")
        stats["job_log"] = tail(jobFile)
        jobFile.close()
      if os.path.exists(outputLogFilePath):
        jobFile = open(outputLogFilePath, "rb")
        stats["output_log"] = tail(jobFile)
        jobFile.close()

      Memcache.set("job_%s" % (self.job.id), stats)
      sleep(0.5)

    if child_pid is None:
      monitor = Monitor(self.child_pid)

    stats = monitor.status()

    if os.path.exists(jobLogFilePath):
      jobFile = open(jobLogFilePath, "rb")
      stats["job_log"] = tail(jobFile)
      jobFile.close()
    if os.path.exists(outputLogFilePath):
      jobFile = open(outputLogFilePath, "rb")
      stats["output_log"] = tail(jobFile)
      jobFile.close()

    Memcache.set("job_%s" % (self.job.id), stats)

    # Record time
    #end_time = time.time()

    #elapsed_time = end_time - start_time
    #print("Elapsed Time: %s seconds" % (elapsed_time))

    #self.job.elapsed_time = elapsed_time

    # Commit the job updates
    DB.session().commit()

    # Perform other scripts
    for task in self.tasks:
      task[1].finish(task[0], jobWorkingPath)

    DB.session().commit()

  def fail(self, job):
    if self.options.verbose:
      print("Failed job #" + str(job.id))

    # Update job to failed
    job.status = "failed"

  def signal(self):
    """ This will be called when the dispatched process wishes to communicate.
    """

    # Determine which file exists
    job = None
    for file in self.files:
      if not os.path.isfile(file[1]):
        job  = file[0]
        path = file[1]
        break

    if job == None:
      print("Signal from unknown job!")
      return

    print("Signal from job %s" % (job.id))

    # Remove that checked file
    self.files[:] = [x for x in self.files if not x[1] == path]

    # Retrieve the child pid
    try:
      spawned = json.load(open("%s/current.json" % (self.options.rootPath)))

      # Log
      self.child_pid = spawned["process_id"]
      print("Message from child [%s]" % (self.child_pid))
    except:
      print("Problem reading %s/current.json." % (self.options.rootPath))

    # Determine which job this child represents

    # Remove log
    try:
      os.remove("%s/current.json" % (self.options.rootPath))
    except:
      print("Problem deleting %s/current.json." % (self.options.rootPath))

    job.log_file = "%s/job-%s.json" % (self.options.rootPath, job.id)

    # Mark job as finished
    self.finish(job)

    # Unlock?
    self.lock_lock.acquire()
    if len(self.files) == 0:
      # Release poll lock to poll once more
      if self.lock.locked():
        self.lock.release()
    self.lock_lock.release()

  def finish(self, job):
    if self.options.verbose:
      print("Finished job #" + str(job.id))

    # Update dependent jobs (if any)
    total_dependencies = (DB.session().query(Job).filter_by(id = DB.session().query(JobDependency.depends_on_job_id).filter_by(job_id = DB.session().query(JobDependency.job_id).filter_by(depends_on_job_id = job.id))).count())

    if total_dependencies == 1:
      master_job = (DB.session().query(Job).filter_by(id = DB.session().query(JobDependency.job_id).filter_by(depends_on_job_id = job.id))).first()
      master_job.has_dependencies = 0
      print("Clearing block to run job #" + str(master_job.id))

    # Update job status to finished
    job.status = "finished"
