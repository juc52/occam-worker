import os
import sys
import json

from time import sleep

from lib.db                import DB
from models.experiment     import Experiment
from models.object         import Object
from models.configuration  import Configuration
from models.input          import Input
from models.input_document import InputDocument
from models.connection     import Connection
from models.workflow       import Workflow
from models.workset        import Workset
from models.result         import Result
from models.job            import Job

from bson.objectid         import ObjectId
from bson.json_util        import dumps

class Runner:
  """This class will handle running experiments and spawning experiment
  processes.
  """

  def __init__(self, options, workingPath):
    """Construct a runner class."""

    self.options     = options
    self.workingPath = workingPath

  def run(self, job):
    """Run the experiment process."""

    experiment = DB.session().query(Experiment).filter_by(id=job.experiment_id).first()
    workset    = DB.session().query(Workset).filter_by(id=experiment.workset_id).first()
    workflow   = DB.session().query(Workflow).filter_by(workset_id=workset.id).first()
    connection = DB.session().query(Connection).filter_by(workflow_id=workflow.id).first()
    object     = DB.session().query(Object).filter_by(id=connection.occam_object_id).first()

    print("Running %s on %s %s in %s" % (experiment.name, object.object_type, object.name, self.workingPath))

    path = object.local_path

    os.chdir(self.workingPath)

    # Dump output.json
    print("Dumping %s runnable object json to %s/object.json" % (object.object_type, self.workingPath))

    # Create the base object
    run_object = {
      "type": "run",
      "name": "job-%s" % (job.id),
      "build": {
        "using": "ubuntu-base-2014-06"
      }
    }

    # Add input data
    input_file = open('object.json', 'w')
    input_data = InputDocument.query().find_one({"_id": ObjectId(job.input_document_id)})
    input_data.pop("_id", None)
    run_object.update(input_data)

    # Write out object
    input_file.write(dumps(run_object))
    input_file.close()

    # Write out any associated input data
    inputs = DB.session().query(Input).filter_by(job_id=job.id)
    for input in inputs:
      # Get corresponding configuration record
      configuration = DB.session().query(Configuration).filter_by(id=input.configuration_id).first()
      configuration_data = InputDocument.query().find_one({"_id": ObjectId(input.input_document_id)})
      configuration_data.pop("_id", None)

      filename = configuration.file
      print(" * Writing out %s as %s" % (configuration.name, configuration.file))

      # TODO: how safe is this?
      input_file = open(filename, 'w')
      input_file.write(dumps(configuration_data))
      input_file.close()

    script = "python %s/spawn.py -q %s -d %s -r %s -j %s -w %s -c '%s/../occam.py build' -c '%s/../occam.py run'" % (
        os.path.dirname(__file__), # Launch spawn.py from occam-worker source
        self.options.rootPath,     # -q {Worker path}
        os.getpid(),               # -d {dispatcher process id}
        self.workingPath,          # -r {job working directory}
        job.id,                    # -j {job id}
        self.workingPath,          # -w {working directory for running job}
        os.path.dirname(__file__), #
        os.path.dirname(__file__)) #

    # Do process spawning/monitoring in a separate space
    os.system(script)

    # Success
    return True

  def finish(self, job, workingPath):
    """Clean up experiment results and store final results information."""

    # TODO: look up output data type to store
    output_path = "%s/output.json" % (workingPath)
    experiment = DB.session().query(Experiment).filter_by(id=job.experiment_id).first()

    if not os.path.exists(output_path):
      sleep(1)

    if os.path.exists(output_path):
      print("Recording output.json to database")
      try:
        output_json = json.load(open(output_path, 'r'))
        record = Result.query().insert(output_json)
        experiment.results_document_id = str(record)
      except:
        print("Problem decoding output.json")
    else:
      print("Output not found at %s" % (output_path))
