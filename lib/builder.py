import os
import sys
import base64

from lib.db           import DB
from models.object    import Object
from models.job       import Job

class Builder:
  def __init__(self, options, workingPath):
    self.options     = options
    self.workingPath = workingPath

  def run(self, job):
    if job.occam_object_id != None:
      obj = DB.session().query(Object).filter_by(id=job.occam_object_id).first()

    print("Building %s in %s" % (obj.name, obj.local_path))

    path = obj.local_path

    os.chdir(self.workingPath)
    occam_opts = ""
    if os.path.isfile("%s/git.txt" % (obj.local_path)):
      f = open("%s/git.txt" % (obj.local_path))
      git = f.readline().rstrip()
      occam_opts = "--source-git %s" % (git)
      f.close()
    elif os.path.isfile("%s/svn.txt" % (obj.local_path)):
      f = open("%s/svn.txt" % (obj.local_path))
      svn = f.readline().rstrip()
      occam_opts = "--source-svn %s" % (svn)
      f.close()

    script = "python %s/spawn.py -q %s -d %s -r %s -j %s -w %s -c '%s/../occam.py build'" % (
        os.path.dirname(__file__), # Launch spawn.py from occam-worker source
        self.options.rootPath,     # -q {Worker path}
        os.getpid(),               # -d {dispatcher process id}
        self.workingPath,          # -r {job working directory}
        job.id,                    # -j {job id}
        path,                      # -w {working directory for running job}
        os.path.dirname(__file__)) # Our library path

    print("Running %s" % (script))

    # Do process spawning/monitoring in a separate space
    os.system(script)

    # Success
    return True

  def finish(self, job, workingPath):
    # Mark object as 'built'
    if job.occam_object_id != None:
      obj = DB.session().query(Object).filter_by(id=job.occam_object_id).first()

    obj.built = 1
