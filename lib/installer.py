import os          # path functions
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling

from lib.db                         import DB
from lib.document_store             import DocumentStore
from models.object                  import Object
from models.system                  import System
from models.output_schema           import OutputSchema
from models.configuration_schema    import ConfigurationSchema
from models.result                  import Result
from models.configuration           import Configuration
from models.configuration_document  import ConfigurationDocument
from models.recipe                  import Recipe
from models.job                     import Job
from models.object_input            import ObjectInput
from models.object_output           import ObjectOutput

class Installer:
  def __init__(self, options, workingPath):
    self.options     = options
    self.workingPath = workingPath

  def do(options, path):
    path = "."

    f = open("%s/object.json" % (path))
    info = json.load(f)
    f.close()

    if not 'install' in info:
      info['install'] = {}

    install_data = info['install']
    install_path = "package"

    if "to" in install_data:
      install_path = install_data["to"]

    install_path = "%s/%s" % (path, install_path)

    # TODO: Use mirrors
    if "git" in install_data:
      repo_path = install_data["git"]
      # TODO: Sanitize
      os.system("git clone %s %s" % (repo_path, install_path))
    elif "hg" in install_data:
      repo_path = install_data["hg"]
      # TODO: Sanitize
      os.system("hg clone %s %s" % (repo_path, install_path))
    elif "tar" in install_data:
      tar_path = install_data["tar"]
      # TODO: Sanitize
      os.system("wget -Nc %s -O %s/source.tar" % (tar_path, path))
      os.system("mkdir -p %s" % (install_path))
      os.system("tar xvf %s/source.tar -C %s" % (path, install_path))

  def run(self, job):
    if job.occam_object_id != None:
      obj    = DB.session().query(Object).filter_by(id=job.occam_object_id).first()
      system = DB.session().query(System).first()

    obj.local_path = system.objects_path

    obj_type = obj.object_type
    name     = obj.name

    obj_type = base64.b32encode(obj_type.encode('utf-8')).lower().replace('='.encode('utf-8'), '-'.encode('utf-8')).decode('utf-8')
    name = base64.b32encode(name.encode('utf-8')).lower().replace('='.encode('utf-8'), '-'.encode('utf-8')).decode('utf-8')

    name = "%s-%s" % (obj_type, name)
    obj.local_path = "%s/%s" % (obj.local_path, obj.id)
    print("Installing %s in %s" % (obj.name, obj.local_path))

    path = obj.local_path

    if not os.path.exists(obj.local_path):
      print(" * Making object directory: %s" % (obj.local_path))
      os.mkdir(obj.local_path)

    script = None
    if obj.package_type == "tar":
      print(" * from tar: %s" % (obj.binary_path))
      command = "-c 'wget -Nc %s -O %s/source.tar' -c 'tar -xvf %s/source.tar -C %s'" % (
          obj.binary_path,            # download tar from
          obj.local_path,             # download tar to
          obj.local_path,             # tar file is here
          obj.local_path)             # untar here

    elif obj.package_type == "git":
      print(" * from %s: %s" % (obj.package_type, obj.binary_path))
      command = "-c 'git clone %s %s/package'" % (
          obj.binary_path,            # install occam scripts from
          obj.local_path)             #    "      "     "     to

    else:
      command = ""

    script = "python %s/spawn.py -q %s -d %s -r %s -j %s -w %s -c 'git clone %s %s' %s" % (
        os.path.dirname(__file__),  # Launch spawn.py from occam-worker source
        self.options.rootPath,      # -q {Worker path}
        os.getpid(),                # -d {dispatcher process id}
        self.workingPath,           # -r {job working directory}
        job.id,                     # -j {job id}
        self.options.objectPath,    # -w {working directory for running job}
        obj.script_path,            # install occam scripts from
        obj.local_path,             #    "      "     "     to
        command)                    # Command to grab source for other objects

    print(" * Running: %s" % (script))

    # Do process spawning/monitoring in a separate space
    os.system(script)

    # Success
    return True

  def rake_recipes(self, job, workingPath, recipePath, obj):
    if os.path.exists(recipePath):
      print(" * Found recipes in %s" % (recipePath))
      # Look for every json file in the recipes path
      for filename in os.listdir(recipePath):
        fullPath = "%s/%s" % (recipePath, filename)
        if os.path.isdir(fullPath):
          self.rake_recipes(job, workingPath, fullPath, obj)
        elif filename[-5:] == '.json':
          print("   * Adding recipe found at %s/%s" % (recipePath, filename))
          # Read json description
          recipe_json = json.load(open("%s/%s" % (recipePath, filename), "r"))

          # Create a recipe for each one
          recipe = Recipe()
          if "name" in recipe_json:
            recipe.name            = recipe_json["name"]
            recipe.occam_object_id = obj.id

            if "section" in recipe_json:
              recipe.section = recipe_json["section"]
            if "description" in recipe_json:
              recipe.description = recipe_json["description"]

            record = ConfigurationDocument.query().insert(DocumentStore.ordered_hash(recipe_json["configuration"]))
            recipe.configuration_document_id = str(record)

          DB.session().add(recipe)

  def finish(self, job, workingPath):
    if job.occam_object_id != None:
      obj = DB.session().query(Object).filter_by(id=job.occam_object_id).first()

    print(" * Finishing install task of %s in %s" % (obj.name, obj.local_path))

    # Open object.json to get the rest of the information we want
    object_json_path = "%s/object.json" % (obj.local_path)
    if os.path.exists(object_json_path):
      object_info = json.load(open(object_json_path, "r"), object_pairs_hook=collections.OrderedDict)

      if "configurations" in object_info:
        configurations = object_info["configurations"]
        if isinstance(configurations, list):
          for configuration in configurations:
            schema      = None
            schema_id   = None
            schema_file = None

            if "schema" in configuration:
              if isinstance(configuration["schema"], dict):
                # Schema is encoded directly in this
                schema = configuration["schema"]
              else:
                # Schema should be in the file given by this field
                schema_file = configuration["schema"]
                schema_file_path = "%s/%s" % (obj.local_path, schema_file)
                if os.path.exists(schema_file_path):
                  schema = json.load(open(schema_file_path, "r"), object_pairs_hook=collections.OrderedDict)
                else:
                  print(" ! Warning: Configuration schema not found: %s" % (schema_file_path))

                # Record schema
                if not schema is None:
                  schema_id = ConfigurationSchema.query().insert(dict(DocumentStore.ordered_hash(schema)))

              # Create configuration record
              record = Configuration()

              # Add the name of the configuration
              if "name" in configuration:
                record.name = configuration["name"]
                print(" * Configuration found: %s" % (record.name));
              else:
                record.name = "General"
                print(" * Configuration found (unnamed)");

              if "file" in configuration:
                record.file = configuration["file"]
              else:
                record.file = "input.json"

              if not schema_file is None:
                record.schema_file = schema_file

              if not schema_id is None:
                record.schema_document_id = str(schema_id)

              record.occam_object_id = obj.id

              # Commit to the database
              DB.session().add(record)
            else:
              print(" ! Warning: Configuration does not have a schema. Configuration skipped.")
        else:
          print(" ! Warning: Configurations is not a list. None will be saved.")
      else:
        print(" * No configurations found.")

      if "inputs" in object_info:
        print(" * Raking information on possible input objects...");
      else:
        print(" * No input objects found.")

      # Get the list of inputs
      if "inputs" in object_info:
        inputs = object_info["inputs"]

        # Coerse to a list of one if the inputs is a single object
        if not isinstance(inputs, list):
          inputs = [inputs]

        # For the list of inputs, store each into the DB
        for input in inputs:
          # Create input record
          record = ObjectInput()

          # Add the name of the configuration
          if "type" in input:
            record.object_type = input["type"]
          else:
            print(" ! Warning: Input object has no given type.")
            record.name = "application/octet-stream"

          if "fifo" in input:
            if input["fifo"]:
              record.fifo = 1
            else:
              record.fifo = 0

          if "group" in input:
            record.object_group = input["group"]
            print(" * Input recorded: %s (group: %s)" % (record.object_type, record.object_group))
          else:
            print(" * Input recorded: %s (no group)" % (record.object_type))

          record.occam_object_id = obj.id

          # Commit to the database
          DB.session().add(record)
      else:
        print(" * No output objects found.")

      # Get the list of outputs
      if "outputs" in object_info:
        print(" * Raking information on possible output objects...");
        outputs = object_info["outputs"]

        # Coerse to a list of one if the outputs is a single object
        if not isinstance(outputs, list):
          outputs = [outputs]

        # For the list of outputs, store each into the DB
        for output in outputs:
          schema      = None
          schema_id   = None
          schema_file = None

          if "schema" in output:
            if isinstance(output["schema"], dict):
              # Schema is encoded directly in this
              schema = output["schema"]
            else:
              # Schema should be in the file given by this field
              schema_file = output["schema"]
              schema_file_path = "%s/%s" % (obj.local_path, schema_file)
              if os.path.exists(schema_file_path):
                schema = json.load(open(schema_file_path, "r"), object_pairs_hook=collections.OrderedDict)
              else:
                print(" ! Warning: Output schema not found: %s" % (schema_file_path))

              # Record schema
              if not schema is None:
                schema_id = OutputSchema.query().insert(dict(DocumentStore.ordered_hash(schema)))

          # Create output record
          record = ObjectOutput()

          # Add the name of the configuration
          if "type" in output:
            record.object_type = output["type"]
          else:
            print(" ! Warning: Output object has no given type.")
            record.object_type = "application/octet-stream"

          if "group" in output:
            record.object_group = output["group"]

          if "file" in output:
            record.file = output["file"]
          else:
            record.file = "output.json"

          if not schema_file is None:
            record.schema_file = schema_file

          if not schema_id is None:
            record.schema_document_id = str(schema_id)

          record.occam_object_id = obj.id

          # Commit to the database
          DB.session().add(record)
      else:
        print(" * No output objects found.")
    else:
      print(" ! Warning: Could not find: %s" % (object_json_path))

    # TODO: Recipes from configurations section
    # Look for recipes
    #print(" * Raking recipes")
    #recipePath = "%s/recipes" % (obj.local_path)
    #self.rake_recipes(job, workingPath, recipePath, obj)
