import json

class Configuration:
  def __init__(self, opts):
    '''
    Creates an instance of a Configuration object for the specified input.
    '''
    self.filename = "input.json"
    self.options = opts

    f = open(self.filename)
    self.info = json.load(f)
    f.close()

    if not (opts.value is None):
      self.info[opts.key] = opts.value

  def filename(self):
    '''
    Returns the filename to be used as the configuration json file.
    '''
    return self.filename

  def hash(self):
    '''
    Returns a dictionary of the configuration info.
    '''
    return self.info

  def write(self):
    '''
    Writes out the configuration to the filename specified when this object
    was constructed.
    '''
    f = open(self.filename, 'w+')
    f.write(json.dumps(self.info))
    f.close()

  def print(self):
    '''
    Prints out the configuration to standard out.
    '''
    self.__print(self.info, "")

  def __print(self, items, indent):
    for k, v in items.items():
      if isinstance(v, dict):
        if (k == self.options.key or self.options.key is None):
          print("%s: " % (k))
          self.__print(v, indent + "  ")
      else:
        if (k == self.options.key or self.options.key is None):
          print("%s%s: %s" % (indent, k, v))
