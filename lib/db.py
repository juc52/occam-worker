from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import create_engine
from sqlalchemy.orm             import sessionmaker

Base = declarative_base()

import yaml

class DB:
  engine = None
  db = None

  def initialize(options):
    if DB.db:
      DB.close()

    print("Initializing connection to database")

    # Look at config.yml
    config_file = open('../occam-web/config/config.yml')
    config = yaml.safe_load(config_file)

    environment = "production"
    if options.development:
      print("Using development environment.")
      environment = "development"
    else:
      print("Using production environment.")

    adapter = config[environment]['database']['adapter']

    if adapter == "sqlite3":
      DB.engine = create_engine('sqlite:///../occam-web/development.db')
    elif adapter == "postgres":
      DB.engine = create_engine('postgres://wilkie:@localhost/occam')

    Session = sessionmaker(bind=DB.engine)

    DB.db = Session()

    print("Database connection established")
    return DB.db

  def close():
    DB.db.close()
    DB.db = None
    DB.engine = None

  def session():
    return DB.db
