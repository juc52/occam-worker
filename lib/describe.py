import json

class Describe:
  def __init__(self, filename):
    try:
      self.filename = filename
      self.info = json.load(open(filename))
    except:
      # Create a description.json later
      self.info = {}

  def name(self, value=None):
    if value != None:
      self.info["name"] = value

    if "name" in self.info:
      return self.info["name"]
    else:
      return ""

  def save(self):
    f = open(self.filename, 'w+')
    f.write(json.dumps(self.info))
