from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

class Recipe(Base):
  __tablename__ = 'recipes'

  id                        = Column(Integer, primary_key=True)
  name                      = Column(String(128))
  section                   = Column(String(128))
  description               = Column(Text)
  occam_object_id           = Column(Integer, ForeignKey('objects.id'))
  configuration_document_id = Column(String(128))
