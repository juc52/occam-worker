from lib.document_store import DocumentStore

class ConfigurationSchema:
  def query():
    return DocumentStore.session().configuration_schemas
