from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

from models.object import Object

class Connection(Base):
  __tablename__ = 'connections'

  id              = Column(Integer, primary_key=True)
  workflow_id     = Column(Integer, ForeignKey('workflow.id'))
  occam_object_id = Column(Integer, ForeignKey('objects.id'))
