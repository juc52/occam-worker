from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

from models.job import Job

class JobDependency(Base):
  __tablename__ = 'job_dependencies'

  id                = Column(Integer, primary_key=True)
  job_id            = Column(Integer, ForeignKey('jobs.id'))
  depends_on_job_id = Column(Integer, ForeignKey('jobs.id'))
