from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, Boolean

from models.job           import Job
from models.configuration import Configuration

class Input(Base):
  __tablename__ = "inputs"

  # Primary key
  id                        = Column(Integer, primary_key=True)

  # The input document foreign key.
  input_document_id        = Column(String(128))

  # The job foreign key that these inputs are for.
  job_id = Column(Integer, ForeignKey('jobs.id'))

  # The configuration foreign key corresponding to these inputs.
  configuration_id = Column(Integer, ForeignKey('configurations.id'))
