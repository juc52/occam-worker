from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, Boolean

from models.object import Object

class Configuration(Base):
  __tablename__ = "configurations"

  # Primary key
  id                        = Column(Integer, primary_key=True)

  # The file that will be created with the configuration options.
  file                      = Column(String(128))

  # The name of this configuration
  name                      = Column(String(128))

  # The file that contains the schema in the script repo.
  schema_file               = Column(String(128))

  # The schema document foreign key.
  schema_document_id        = Column(String(128))

  # The OCCAM object foreign key that these configurations are for.
  occam_object_id = Column(Integer, ForeignKey('objects.id'))
