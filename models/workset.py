from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

from models.object   import Object

class Workset(Base):
  __tablename__ = 'worksets'

  id            = Column(Integer, primary_key=True)
