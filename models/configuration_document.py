from lib.document_store import DocumentStore

class ConfigurationDocument:
  def query():
    return DocumentStore.session().configurations
