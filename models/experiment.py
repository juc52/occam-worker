from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

from models.account  import Account
from models.object   import Object
from models.workset  import Workset

class Experiment(Base):
  __tablename__ = 'experiments'

  id                        = Column(Integer, primary_key=True)
  name                      = Column(String(128))
  tags                      = Column(String(128))
  forked_from_id            = Column(Integer, ForeignKey('experiments.id'))
  account_id                = Column(Integer, ForeignKey('accounts.id'))
  workset_id                = Column(Integer, ForeignKey('worksets.id'))
  configuration_document_id = Column(String(128))
  results_document_id       = Column(String(128))
