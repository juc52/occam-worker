from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, Boolean

from models.object import Object

class ObjectOutput(Base):
  __tablename__ = 'object_outputs'

  # Primary key
  id                        = Column(Integer, primary_key=True)

  # The type of object
  object_type               = Column(String(128))

  # The group for this object
  object_group              = Column(String(128))

  # The file that will be created with the configuration options.
  file                      = Column(String(128))

  # The file that contains the schema in the script repo.
  schema_file               = Column(String(128))

  # The schema document foreign key.
  schema_document_id        = Column(String(128))

  # The OCCAM object foreign key that these configurations are for.
  occam_object_id           = Column(Integer, ForeignKey('objects.id'))

  # Whether or not this is a corunning object
  fifo                      = Column(Integer)
