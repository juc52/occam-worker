from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

from models.object   import Object

from models.experiment import Experiment
from models.workset    import Workset

class Workflow(Base):
  __tablename__ = 'workflows'

  id            = Column(Integer, primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiment.id'))
  workset_id    = Column(Integer, ForeignKey('workset.id'))
