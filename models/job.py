from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

from models.experiment import Experiment

class Job(Base):
  __tablename__ = 'jobs'

  id                = Column(Integer, primary_key=True)
  experiment_id     = Column(Integer, ForeignKey('experiments.id'))
  codependant       = Column(Integer)
  has_dependencies  = Column(Integer)
  input_document_id = Column(String(128))
  status            = Column(String)
  kind              = Column(String)
  occam_object_id   = Column(Integer, ForeignKey('objects.id'))
  log_file          = Column(String)
  elapsed_time      = Column(Float)
