from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, Boolean

class Object(Base):
  __tablename__ = 'objects'

  id                        = Column(Integer, primary_key=True)
  name                      = Column(String(128))
  object_type               = Column(String(128))
  tags                      = Column(String(128))
  local_path                = Column(String(128))
  binary_path               = Column(String(128))
  package_type              = Column(String(128))
  script_path               = Column(String(128))
  built                     = Column(Integer)
