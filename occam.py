#!/usr/bin/env python

# Lib: Simulator (lists/edits simulator description)
# Lib: Build     (build details) - generate docker file
# Lib: Runner    (can run the simulator through docker or natively)
# Lib: Option    (lists/edits job options)

# occam describe name=sniper
# name: sniper

# occam describe
import sys

from lib.describe      import Describe
from lib.docker        import Docker
from lib.installer     import Installer
from lib.configuration import Configuration

from optparse      import OptionParser
import json
from bson.json_util       import dumps

class Usage(Exception):
  def __init__(self, msg):
    self.msg = msg

def main(argv=None):
  if argv is None:
    argv = sys.argv

  if argv[1] == "describe":
    parser = OptionParser("occam describe")
    parser.add_option("-v", "--verbose",  action = "store_true",
                                          dest   = "verbose",
                                          help   = "prints all messages")
    opts, args = parser.parse_args()
    describe = Describe("object.json")
    print(describe.name())
    print(describe.name("foo"))
    describe.save()
  elif argv[1] == "new":
    parser = OptionParser("occam new")
    opts, args = parser.parse_args()
    schema = json.load(open("input_schema.json"))
    obj_info = json.load(open("object.json"))
    # generate default input.json
    config = {
    }
    def parse(d, current):
      for k, v in d.items():
        if isinstance(v, dict) and not "type" in v or isinstance(v["type"], dict):
          # Group
          current[k] = {}
          parse(v, current[k])
        else:
          # Item
          if "default" in v:
            if "type" in v:
              if v["type"] == "int":
                v["default"] = int(v["default"])
              elif v["type"] == "float":
                v["default"] = float(v["default"])
            current[k] = v["default"]
    parse(schema, config)
    input_json = {
      "input": {
        obj_info["type"]: [
          {
            "id": "0",
            "name": obj_info["name"],
            "type": obj_info["type"],
            "configuration": config
          }
        ]
      },
      "run": {
      }
    }
    input_file = open('input.json', 'w')
    input_file.write(dumps(input_json))
  elif argv[1] == "console":
    parser = OptionParser("occam console OBJECT_TYPE NAME")
    parser.add_option("-v", "--volume",  action = "store",
                                         dest   = "volume",
                                         help   = "mount a directory from the host machine within this session")
    parser.add_option("-j", "--job",     action = "store_true",
                                         dest   = "job",
                                         help   = "mount the current directory from the host machine within this session as /job")
    opts, args = parser.parse_args()

    if len(args) > 2:
      obj_type = args[1]
      name = args[2]
    elif len(args) > 1:
      obj_type = None
      name = args[1]
    else:
      obj_type = None
      name = None

    Docker.console(opts, obj_type, name)
  elif argv[1] == "run":
    parser = OptionParser("occam run TYPE NAME")
    opts, args = parser.parse_args()

    # Look for object.json?
    obj_info = json.load(open("object.json"))
    if "type" in obj_info:
      type = obj_info["type"]
    else:
      type = "simulator"

    if len(args) > 2:
      obj_type = args[1]
      name = args[2]
    elif len(args) > 1:
      obj_type = None
      name = args[1]
    else:
      obj_type = None
      name = None

    Docker.run(obj_type, name)
  elif argv[1] == "simulators":
    Docker.list('simulator')
  elif argv[1] == "install":
    parser = OptionParser("occam install TYPE NAME")
    parser.add_option("-g", "--git",  action = "store",
                                      dest   = "git",
                                      help   = "git URL for occam package")
    parser.add_option("-t", "--tar",  action = "store",
                                      dest   = "tar",
                                      help   = "tar URL for occam package")
    parser.add_option("-m", "--hg",   action = "store",
                                      dest   = "hg",
                                      help   = "mercurial URL for occam package")
    parser.add_option("-s", "--svn",  action = "store",
                                      dest   = "svn",
                                      help   = "svn URL for occam package")
    parser.add_option("-G", "--source-git",  action = "store",
                                             dest   = "source_git",
                                             help   = "git URL for simulator source")
    parser.add_option("-T", "--source-tar",  action = "store",
                                             dest   = "source_tar",
                                             help   = "tar URL for simulator source")
    parser.add_option("-M", "--source-hg",   action = "store",
                                             dest   = "source_hg",
                                             help   = "mercurial URL for simulator source")
    parser.add_option("-S", "--source-svn",  action = "store",
                                             dest   = "source_svn",
                                             help   = "svn URL for simulator package")
    parser.add_option("-v", "--verbose",  action = "store_true",
                                          dest   = "verbose",
                                          help   = "prints all messages")

    opts, args = parser.parse_args()
    if len(args) > 2:
      obj_type = args[1]
      name = args[2]
    elif len(args) > 1:
      obj_type = None
      name = args[1]
    else:
      obj_type = None
      name = None

    Installer.do(opts, name)
  elif argv[1] == "build":
    parser = OptionParser("occam build TYPE NAME")
    parser.add_option("-g", "--git",  action = "store",
                                      dest   = "git",
                                      help   = "git URL for occam package")
    parser.add_option("-t", "--tar",  action = "store",
                                      dest   = "tar",
                                      help   = "tar URL for occam package")
    parser.add_option("-m", "--hg",   action = "store",
                                      dest   = "hg",
                                      help   = "mercurial URL for occam package")
    parser.add_option("-s", "--svn",  action = "store",
                                      dest   = "svn",
                                      help   = "svn URL for occam package")
    parser.add_option("-G", "--source-git",  action = "store",
                                             dest   = "source_git",
                                             help   = "git URL for simulator source")
    parser.add_option("-T", "--source-tar",  action = "store",
                                             dest   = "source_tar",
                                             help   = "tar URL for simulator source")
    parser.add_option("-M", "--source-hg",   action = "store",
                                             dest   = "source_hg",
                                             help   = "mercurial URL for simulator source")
    parser.add_option("-S", "--source-svn",  action = "store",
                                             dest   = "source_svn",
                                             help   = "svn URL for simulator package")
    parser.add_option("-v", "--verbose",  action = "store_true",
                                          dest   = "verbose",
                                          help   = "prints all messages")

    opts, args = parser.parse_args()
    if len(args) > 2:
      obj_type = args[1]
      name = args[2]
    elif len(args) > 1:
      obj_type = None
      name = args[1]
    else:
      obj_type = None
      name = None

    Docker.build(opts, obj_type, name)
  elif argv[1] == "install":
    name = argv[2]
    parser = OptionParser("occam install GIT_URL")
  elif argv[1] == "list":
    name = argv[2]
    parser = OptionParser("occam list")
    opts, args = parser.parse_args()
    if len(args) > 1:
      obj_type = args[1]

    Docker.list(opts, obj_type)
  elif argv[1] == "config":
    parser = OptionParser("occam config [KEY] [VALUE]")
    opts, args = parser.parse_args()

    if len(argv) > 2:
      opts.key = argv[2]
    else:
      opts.key = None

    if len(argv) > 3:
      opts.value = argv[3]
    else:
      opts.value = None

    config = Configuration(opts)
    config.print()

    if not (opts.value is None):
      config.write()

  return 0

if __name__ == "__main__":
  sys.exit(main())
